FROM gradle:7-jdk11 as gradle

COPY src src
COPY *.gradle ./

RUN gradle build

FROM openjdk:11 as jdk

RUN apt-get update && apt-get install -y tini

ENTRYPOINT [ "tini", "-g", "--" ]

COPY --from=gradle /home/gradle/build/libs/*.jar ./app.jar

EXPOSE 8080

CMD [ "sh", "-c", "java -jar app.jar" ]
