package com.trial.shortlink.data.repository;

import com.trial.shortlink.data.models.Shortlink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDateTime;

/**
 * DAO (JPA repository) for for working with database.
 */
@Repository
public interface ShortlinkRepository extends JpaRepository<Shortlink, Integer> {

    /**
     * Find entities of shortlink by it's short link and by checking TTL
     * @param shortlink
     * @param nowDate
     * @return List of entities (may be is empty)
     */
    List<Shortlink> findByShortlinkAndExpiredAtGreaterThan(String shortlink, LocalDateTime nowDate);

    /**
     * Find entities of shortlink by it's key and by checking TTL
     * @param key
     * @param nowDate
     * @return List of entities (may be is empty)
     */
    List<Shortlink> findByKeyAndExpiredAtGreaterThan(String key, LocalDateTime nowDate);
}
