package com.trial.shortlink.data.models;

import java.time.LocalDateTime;
import javax.persistence.*;

/**
 * ORM model for shortlink
 */
@Entity
@Table(name = "shortlink")
public class Shortlink {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String key;
    private String shortlink;
    private String longlink;
    private LocalDateTime expiredAt;

    public Shortlink(){}

    public Integer getId() {
        return id;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getShortlink() {
        return shortlink;
    }
    public void setShortlink(String shortlink) {
        this.shortlink = shortlink;
    }
    public String getLonglink() {
        return longlink;
    }
    public void setLonglink(String longlink) {
        this.longlink = longlink;
    }
    public void setExpiredAt(LocalDateTime expiredAt) {
        this.expiredAt = expiredAt;
    }
    public LocalDateTime getExpiredAt() {
        return expiredAt;
    }

    @Override
    public String toString() {
        return "Shortlink(" + "id=" + id + "){" + key + ':' + longlink + '}';
    }
}
