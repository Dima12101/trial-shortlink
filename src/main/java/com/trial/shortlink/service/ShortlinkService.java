package com.trial.shortlink.service;

import com.trial.shortlink.data.models.Shortlink;
import com.trial.shortlink.data.repository.ShortlinkRepository;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Implementation of Shortlink service
 */
@Service
public class ShortlinkService implements IShortlinkService {

    @Autowired
    private ShortlinkRepository shortlinkRepository;

    private static final int expiringMinutes = 10;

    @Override
    public String convertLink(String link) throws NoSuchAlgorithmException {
		// Set up MD5
        MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		
        // Convert
        m.update(link.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
        return bigInt.toString(16);
    }

    @Override
    public Shortlink createShortlink(String longlink, String key){
        // Create shortlink
        Shortlink shortlink = new Shortlink();
        shortlink.setKey(key);
        shortlink.setShortlink(System.getenv("APP_DOMAIN") + '/' + key);
        shortlink.setLonglink(longlink);
        shortlink.setExpiredAt(LocalDateTime.now().plusMinutes(expiringMinutes));

        // Save
		shortlinkRepository.save(shortlink);

        return shortlink;
    }

    @Override
    public String recoverLinkByShortlink(String shortlink){
        List<Shortlink> shortlinks = shortlinkRepository
            .findByShortlinkAndExpiredAtGreaterThan(shortlink, LocalDateTime.now());
        return shortlinks.isEmpty() ? null : shortlinks.get(0).getLonglink();
    }

    @Override
    public String recoverLinkByKey(String key){
        List<Shortlink> shortlinks = shortlinkRepository
            .findByKeyAndExpiredAtGreaterThan(key, LocalDateTime.now());
        return shortlinks.isEmpty() ? null : shortlinks.get(0).getLonglink();
    }
}
