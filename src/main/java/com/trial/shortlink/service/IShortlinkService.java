package com.trial.shortlink.service;

import com.trial.shortlink.data.models.Shortlink;

import java.security.NoSuchAlgorithmException;

/**
 * Interface of Shortlink service
 */
public interface IShortlinkService {

    /**
     * MD5-based link conversion algorithm.
     * @param input link
     * @return key of short link.
     */
    String convertLink(String link) throws NoSuchAlgorithmException;

    /**
     * Creating a short link based on the generated key.
     * @param longlink - original link
     * @param key - key of short link
     * @return instance of Shortlink entity.
     */
    Shortlink createShortlink(String longlink, String key);

    /**
     * Recover original link by short link.
     * @param shortlink
     * @return Original link or null if it failed.
     */
    String recoverLinkByShortlink(String shortlink);

    /**
     * Recover original link by key of short link.
     * @param key of short link
     * @return Original link or null if it failed.
     */
    String recoverLinkByKey(String key);
}
