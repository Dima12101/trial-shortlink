package com.trial.shortlink.web;

import com.trial.shortlink.data.models.Shortlink;
import com.trial.shortlink.service.IShortlinkService;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;

import java.security.NoSuchAlgorithmException;

import javax.validation.constraints.*;


/**
 * Spring Rest Controller for REST API.
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Validated
public class ShortlinkController {

	@Autowired
    IShortlinkService shortlinkService;


    /**
     * Endpoint for converting link to shortlink (create shortlink).
     * @param longlink - original link for converting
     * @return converted short version of the link.
	 * @throws ConstraintViolationException throws if there is an error in the request structure
	 * @throws MissingServletRequestParameterException throws if there is an error in the request structure
     */
	@ApiResponse(responseCode = "201", description = "Successfully converted link")
	@PostMapping("/convert")
	public ResponseEntity<Shortlink> createShortlink( 
		@RequestParam("longlink") @NotEmpty @NotNull String longlink) throws NoSuchAlgorithmException {
        return new ResponseEntity<>(
			shortlinkService.createShortlink(
				longlink, 
				shortlinkService.convertLink(longlink)), 
			HttpStatus.CREATED);
    }

	/**
     * Endpoint for recovering shortlink to original link.
     * @param shortlink - converted short version of the original link
     * @return recovered original version of the link.
	 * @throws ResponseStatusException throws when short version link could not be found:
     * does not exist or expired
	 * @throws ConstraintViolationException throws if there is an error in the request structure
	 * @throws MissingServletRequestParameterException throws if there is an error in the request structure
     */
    @ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					content = @Content(schema = @Schema(implementation = String.class)),
					description = "Found available link")
	})
	@GetMapping("/recover")
	public ResponseEntity<String> getLonglink(
		@RequestParam("shortlink") @NotEmpty @NotNull String shortlink) {
		String link = shortlinkService.recoverLinkByShortlink(shortlink);
		if (link == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Shortlink not found or expired");
		}
		return new ResponseEntity<>(link, HttpStatus.OK);
	}


	/**
     * Endpoint for redirection to the original destination.
     * @param key - key of shortlink (<domain>/<key>)
     * @return redirection view.
	 * @throws ResponseStatusException throws when short version link could not be found:
     * does not exist or expired
	 * @throws ConstraintViolationException throws if there is an error in the request structure
	 * @throws MissingServletRequestParameterException throws if there is an error in the request structure
     */
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "200",
					content = @Content,
					description = "Redirects to original link")
	})
	@GetMapping("/{keyLink}")
	public RedirectView redirectToLonglink(
		@PathVariable("keyLink") @NotEmpty String keyLink) {
		String link = shortlinkService.recoverLinkByKey(keyLink);
		if (link == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Shortlink not found or expired");
		}
		return new RedirectView(link);
	}
}
