package com.trial.shortlink.web.exception;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolationException;

/**
 * Global exceptions handler.
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    
    /**
     * Error handling in the request structure (constraints of parameters)
	 * and show error message to user.
     * @param Thrown exception
     * @return Response containing error message and http status
     */
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "400",
					content = @Content(schema = @Schema(implementation = String.class)),
					description = "Not valid due to validation error"),
	})
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
		return new ResponseEntity<>("Not valid due to validation error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	}

    /**
     * Error handling in the request structure (missing parameters)
	 * and show error message to user.
     * @param Thrown exception
     * @return Response containing error message and http status
     */
	@ApiResponses(value = {
			@ApiResponse(
					responseCode = "400",
					content = @Content(schema = @Schema(implementation = String.class)),
					description = "Not valid due to validation error")
	})
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
		return new ResponseEntity<>("not valid due to validation error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	}

	/**
     * Handling exception of business logic
	 * and show error message to user.
     * @param Thrown exception
     * @return Response containing error message and http status
     */
	@ApiResponses(value = {
			@ApiResponse(
				responseCode = "404",
				content = @Content(schema = @Schema(implementation = String.class)),
				description = "Shortlink not found or expired")
	})
	@ExceptionHandler(ResponseStatusException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<String> handleResponseStatusException(ResponseStatusException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}
}
